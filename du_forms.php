<?php

namespace du{
  
  class forms{

    function __construct() {
    }

    /**
     * Convert a slug to a standard name
     *
     * @return void
     * @author mohammad@duable.com
     **/
    function namify( $slug ){
      return ucwords( str_replace( '_', ' ', $slug ) );
    }

    function errors(){
      # Will hold global variable safely
      static $wp_error; 
      return isset( $wp_error ) ? $wp_error : ( $wp_error = new \WP_Error(null, null, null ) );
    }

    function field_error( $form_prefix, $field ) {
      $field_error = false;
      if ( $codes = $this->errors()->get_error_codes() ) {
        foreach( $codes as $code ){
          if ( 0 === strpos( $code, $form_prefix . $field ) ) {
            $message = $this->errors()->get_error_message( $code );
            $field_error .= '<span class="error">' . $message . '</span><br/>'; 
          }
        }
      }
      return $field_error;
    }

    function show_error_messages(){
      if ( $codes = $this->errors()->get_error_codes() ) {
      echo '<div class="form-errors">';
        # Loop error codes and display errors
        foreach( $codes as $code ){
          $message = $this->errors()->get_error_message( $code );
          echo '<span class="error"><strong>' . __('Error') . '</strong>: ' . $message . '</span><br/>';
        }
        echo '</div>';
      } 
    }

    function field_html( $prefix, $field, $options, $current_value ) {

      # Start field HTML
      ?>
      <span class="form-field<?php if ( $this->field_error( $prefix, $field ) ) echo ' error'; ?>" field-type="<?php echo $options[ 'type' ]; ?>" field-name="<?php echo $field; ?>">
      <?php 
        if ( @$options[ 'show_label' ] == true ) { ?>
        <label for="<?php echo $prefix . $field; ?>"><?php _e( $this->namify( $field ) ); ?>:</label><?php 
        }

      # Output specific fields based on type
      switch ( $options[ 'type' ] ) {
        
        case 'select' : ?>
        <span class="select-wrapper"<?php if ( $options[ 'required' ] == true ) echo ' required'; ?>>
          <select name="<?php echo $prefix . $field; ?>" id="<?php echo $prefix . $field; ?>" <?php if ( $options[ 'required' ] == true ) echo 'required'; ?> placeholder="<?php echo $options[ 'desc' ]; ?>">
          <?php
            if ( empty( $value ) ) { ?>
              <option value="" disabled selected style="display: none">Select <?php echo $options[ 'desc' ]; ?></option>
            <?php }
            foreach ( $options[ 'options' ] as $value => $name ) { ?>
              <option value="<?php echo $value; ?>"<?php if ( $current_value == $value ) echo ' selected'; ?>><?php echo $name; ?></option><?php
            } ?>
          </select>
        </span><?php
          break;

        case 'password' : ?>
          <input name="<?php echo $prefix . $field; ?>" id="<?php echo $prefix . $field; ?>" <?php if ( $options[ 'required' ] == true ) echo 'required'; ?> type="<?php echo $options[ 'type' ]; ?>" value="<?php echo $current_value; ?>" placeholder="<?php echo $options[ 'desc' ]; ?>" />
          <?php
          if ( @$options[ 'show_req' ] ) {
            echo "<span class=\"$field-notes field-notes\">";
            echo "Password must be 8 characters long, contain at least one capital letter, and at least one number.";
            echo "</span>";
          }
          break;

        default : ?>
          <input name="<?php echo $prefix . $field; ?>" id="<?php echo $prefix . $field; ?>" <?php if ( $options[ 'required' ] == true ) echo 'required'; ?> type="<?php echo $options[ 'type' ]; ?>" value="<?php echo $current_value; ?>" placeholder="<?php echo $options[ 'desc' ]; ?>" />
          <?php
          break;

      }

      # Show error for this field if exists
      if ( $this->field_error( $prefix, $field ) ) {
        echo '<span class="alert error ' . $field . '">';
        echo $this->field_error( $prefix, $field );
        echo '</span>';
      } ?> 
      </span> <?php
    }
  }

}